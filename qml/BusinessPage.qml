import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0
//import QtQuick.Dialogs 1.2
import Qt.labs.platform 1.0
import QtQuick.Controls 2.15

Page {
    id: root
    anchors.fill: parent

    CurrenciesModel {
        id: currenciesModel
    }

    onVisibleChanged: {
        businessInfoModel.loadInfo();
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            id: toolBarLayout
            width: parent.width

            ToolButton {
                id: toolButton
                icon.source: "../icons/menu_64x64.png"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    drawer.open()
                }
            }

            Label {
                text: qsTr("Business")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            ToolButton {
                id: btnExportReport
                icon.source: "../icons/check_32x32.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    businessInfoModel.saveInfo();
                    popPage()
                }
            }
        }
    }

    BusinessForm {
        id: businessForm
    }
}
