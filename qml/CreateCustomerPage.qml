import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal completed(int id, string name);

    CustomerModel {
        id: customerModel
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            width: parent.width
            ToolButton {
                id: btnBack
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    popPage()
                }
            }

            Label {
                text: qsTr("New customer")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            ToolButton {
                icon.source: "../icons/check_32x32.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    if(customerForm.customerModel.name === "")
                        showMessage("Customer name is required.")
                    else
                    {
                        customerForm.customerModel.saveCustomer()
                        root.completed(customerForm.customerModel.customerID, customerForm.customerModel.name)
                        popPage()
                    }
                }
            }
        }
    }

    CustomerForm {
        id: customerForm
        customerModel: CustomerModel {
        }
        onCompleted: root.vompleted()
    }
}
