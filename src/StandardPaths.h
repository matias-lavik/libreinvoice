#ifndef STANDARDPATHS_H
#define STANDARDPATHS_H

#include <QString>

class StandardPaths
{
public:
    StandardPaths();

    static QString getAssetsPath();
    static QString getReportsPath();
};

#endif // STANDARDPATHS_H
