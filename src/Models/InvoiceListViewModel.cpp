#include "InvoiceListViewModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlRecord>

const QString SQL_STRING_BASE = "select id, invoiceDate, dueDate, description, customerID, isPaid, (select sum(invoiceItems.itemPrice * invoiceItems.quantity * (1.0 - invoiceItems.discount / 100.0) * (1.0 + invoiceItems.tax / 100.0)) from invoiceItems where invoiceItems.invoiceID = invoices.id) as invoiceSum, (select name from customers where customers.id = invoices.customerID) as customerName from invoices";

InvoiceListViewModel::InvoiceListViewModel(QObject *parent) : QSqlQueryModel(parent)
{
    showAll();
}

QVariant InvoiceListViewModel::data(const QModelIndex &index, int role) const
{
    if(role < Qt::UserRole)
        return QSqlQueryModel::data(index, role);
    QSqlRecord r = record(index.row());
    QVariant var = r.value(QString(roleNames().value(role))).toString();
    return var;
}

QHash<int, QByteArray> InvoiceListViewModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Qt::UserRole] = "id";
    names[Qt::UserRole + 1] = "invoiceDate";
    names[Qt::UserRole + 2] = "dueDate";
    names[Qt::UserRole + 3] = "description";
    names[Qt::UserRole + 4] = "invoiceSum";
    names[Qt::UserRole + 5] = "customerID";
    names[Qt::UserRole + 6] = "isPaid";
    names[Qt::UserRole + 7] = "customerName";
    return names;
}

void InvoiceListViewModel::showAll()
{
    mFilter = InvoiceFilter::All;
    refresh();
}

void InvoiceListViewModel::showUnpaid()
{
    mFilter = InvoiceFilter::Unpaid;
    refresh();
}

void InvoiceListViewModel::showPaid()
{
    mFilter = InvoiceFilter::Paid;
    refresh();
}

void InvoiceListViewModel::setSearchString(QString searchString)
{
    mSearchString = searchString;
    refresh();
}

void InvoiceListViewModel::refresh()
{
    QList<QString> filters;
    if(mFilter == InvoiceFilter::Paid)
        filters.append("isPaid = 1");
    else if(mFilter == InvoiceFilter::Unpaid)
        filters.append("isPaid = 0");

    if(mSearchString != "")
        filters.append(QString("description like \"\%%1\%\" or (select customers.name from customers where customers.id = customerID limit 1) like \"\%%1\%\"").arg(mSearchString, mSearchString));

    QString sqlString = SQL_STRING_BASE;
    for(int i = 0; i < filters.size(); i++)
    {
        sqlString += i == 0 ? " where " : " and ";
        sqlString += filters[i];
    }

    setQuery(sqlString, GApp->getUserDatabase()->getSqlDatabase());
}
