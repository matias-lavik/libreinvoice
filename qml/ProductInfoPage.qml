import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal completed();

    ProductModel {
        id: productModel
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            Layout.alignment: Qt.AlignLeft
            width: parent.width
            ToolButton {
                id: btnBack
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if(productForm.productModel.isModified())
                        showPromptYesNo(qsTr("Do you want to save your changes?"), saveAndClose, closeWithoutSaving)
                    else
                        popPage()
                }
            }
            Label {
                id: productNameLabel
                text: productForm.productModel.productName
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }
            ToolButton {
                icon.source: "../icons/more_64x64.png"
                icon.color: transparent
                Layout.alignment: Qt.AlignRight
                onClicked: optionsPopup.open()
            }
        }
    }

    ProductForm {
        id: productForm
        productModel: ProductModel {
        }
    }

    Drawer {
        id: optionsPopup
        edge: Qt.RightEdge
        width: 150
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Column {
            width: parent.width
            ToolButton {
                width: parent.width
                text: qsTr("Save")
                onClicked: {
                    saveAndClose()
                }
            }
            ToolButton {
                width: parent.width
                text: qsTr("Delete")
                onClicked: {
                    productForm.productModel.deleteProduct()
                    optionsPopup.close()
                    popPage()
                }
            }
        }
    }

    function loadProduct(productID) {
        productForm.productModel.loadProduct(productID)
        productForm.setFieldsFromModel()
    }

    function saveAndClose() {
        productForm.productModel.saveProduct()
        popPage()
    }

    function closeWithoutSaving() {
        popPage()
    }
}
