import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0
import QtQuick.Controls 1.4
import Qt.labs.platform 1.0
import QtQuick.Controls 2.15

Page {
    id: root
    anchors.fill: parent

    CurrenciesModel {
        id: currenciesModel
    }

    onVisibleChanged: {
        businessInfoModel.loadInfo();
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            id: toolBarLayout
            Layout.alignment: Qt.AlignLeft
            width: parent.width

            ToolButton {
                id: btnBack
                text: tabView.currentIndex > 0 ? "←" : ""
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                //visible: tabView.currentIndex > 0
                onClicked: {
                    if(tabView.currentIndex > 0)
                        tabView.currentIndex--
                }
            }

            Label {
                text: "Setup"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }

            ToolButton {
                id: btnNext
                text: tabView.currentIndex == tabView.count - 1 ? qsTr("Done") : qsTr("Next")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    if(tabView.currentIndex == tabView.count - 1)
                    {
                        businessInfoModel.saveInfo()
                        popPage()
                        pushPage("CustomerPage.qml")
                    }
                    else
                        tabView.currentIndex++
                }
            }
        }
    }

    TabView {
        id: tabView
        anchors.fill: parent
        tabsVisible: false
        frameVisible: false
        Tab {
            anchors.fill: parent
            ColumnLayout {
                anchors.fill: parent
                //spacing: 10
                Label {
                    Layout.topMargin: 10
                    Layout.alignment: Qt.AlignCenter
                    text: qsTr("Select language.")
                    font.pixelSize: Qt.application.font.pixelSize * 2.0
                }
                LanguageSelectionScreen {
                    Layout.alignment: Qt.AlignHCenter
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }
        }
        Tab {
            anchors.fill: parent
            ColumnLayout {
                anchors.fill: parent
                Label {
                    Layout.topMargin: 10
                    Layout.alignment: Qt.AlignHCenter
                    text: qsTr("Fill in information about your business")
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    BusinessForm {
                        id: businessForm
                    }
                }
            }
        }
    }
}
