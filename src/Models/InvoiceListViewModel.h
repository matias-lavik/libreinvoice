#ifndef INVOICELISTVIEWMODEL_H
#define INVOICELISTVIEWMODEL_H

#include <QSqlTableModel>

class InvoiceListViewModel : public QSqlQueryModel
{
    Q_OBJECT

private:
    enum class InvoiceFilter
    {
        Paid, Unpaid, All
    };

private:
    QString mSearchString = "";
    InvoiceFilter mFilter = InvoiceFilter::All;

public:
    explicit InvoiceListViewModel(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void showAll();
    Q_INVOKABLE void showUnpaid();
    Q_INVOKABLE void showPaid();
    Q_INVOKABLE void setSearchString(QString searchString);
    Q_INVOKABLE void refresh();
};

#endif // INVOICELISTVIEWMODEL_H
