#include "App.h"
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QSqlQuery>

App* GApp = nullptr;

App::App()
{

}

void App::initialiseApp(QApplication* app, QQmlApplicationEngine* engine)
{
    mQtApp = app;
    mAppEngine = engine;
    mUserDatabase = new UserDatabase();

    //QString dbLoc = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString dbLoc = QDir::currentPath();
    QString userDbLoc = QDir(dbLoc).filePath("userdb.sqlite");

    bool dbLoaded = false;
    if(QFile::exists(userDbLoc))
        dbLoaded = mUserDatabase->loadDatabase(userDbLoc, "user_db");
    if(!dbLoaded)
        dbLoaded = mUserDatabase->createDatabase(userDbLoc, "user_db");

    QSqlQuery query(GApp->getUserDatabase()->getSqlDatabase());
    query.prepare("select language from settings");
    query.exec();
    query.first();
    setLanguage(query.value(0).toString());
}

void App::setLanguage(QString langCode)
{
    QString locFilePath = QString(":/localisation/") + langCode+ QString(".qm");
    QTranslator* trans = new QTranslator();
    if(!trans->load(locFilePath))
    {
        qDebug() << "Failed to load language";
        delete trans;
    }
    else
    {
        mLangauge = langCode;
        if(mTranslator != nullptr)
        {
            mQtApp->removeTranslator(mTranslator);
            delete mTranslator;
        }
        mTranslator = trans;
        mQtApp->installTranslator(mTranslator);
        mAppEngine->retranslate();

        QSqlQuery updateQuery(mUserDatabase->getSqlDatabase());
        updateQuery.prepare("update settings set language = ? where id = 1");
        updateQuery.addBindValue(mLangauge);
        updateQuery.exec();
    }
}

void App::clearDatabase()
{
    QString dbDir = QDir::currentPath();
    QString userDbLoc = QDir(dbDir).filePath("userdb.sqlite");
    QString backupLoc = QDir(dbDir).filePath("userdb_backup.sqlite");
    QFile::remove(backupLoc);
    QFile::copy(userDbLoc, backupLoc);
    QFile::remove(userDbLoc);
}
