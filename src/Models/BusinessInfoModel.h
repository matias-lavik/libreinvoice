#ifndef BusinessInfoModel_H
#define BusinessInfoModel_H

#include <QObject>
#include <QString>
#include <QImage>

class BusinessInfoModel : public QObject
{
    Q_OBJECT
public:
    explicit BusinessInfoModel(QObject *parent = nullptr);

private:
    QString mName;
    QString mVatNumber;
    QString mAddress;
    QString mPhoneNumber;
    QString mEmail;
    QString mPostalCode;
    QString mPostalArea;
    QString mCountry;
    QString mLogoPath;
    QString mLogoBase64;
    int mCurrencyID;
    QString mCurrencyCode;
    QString mCurrencySymbol;
    QString mPaymentAccount;
    QString mPayment2;

    Q_PROPERTY (QString name READ getName WRITE setName NOTIFY nameChangedEvent)
    Q_PROPERTY (QString vatNumber READ getVatNumber WRITE setVatNumber NOTIFY vatNumberChangedEvent)
    Q_PROPERTY (QString address READ getAddress WRITE setAddress NOTIFY addressChangedEvent)
    Q_PROPERTY (QString phoneNumber READ getPhoneNumber WRITE setPhoneNumber NOTIFY phoneNumberChangedEvent)
    Q_PROPERTY (QString email READ getEmail WRITE setEmail NOTIFY emailChangedEvent)
    Q_PROPERTY (QString postalCode READ getPostalCode WRITE setPostalCode NOTIFY postalCodeChangedEvent)
    Q_PROPERTY (QString postalArea READ getPostalArea WRITE setPostalArea NOTIFY postalAreaChangedEvent)
    Q_PROPERTY (QString country READ getCountry WRITE setCountry NOTIFY countryChangedEvent)
    Q_PROPERTY (QString logoPath READ getLogoPath WRITE setLogoPath NOTIFY logoPathChangedEvent)
    Q_PROPERTY (QString logoBase64 READ getLogoBase64 NOTIFY logoBase64ChangedEvent)
    Q_PROPERTY (int currencyID READ getCurrencyID WRITE setCurrencyID NOTIFY currencyIDChangedEvent)
    Q_PROPERTY (QString currencyCode READ getCurrencyCode NOTIFY currencyCodeChangedEvent)
    Q_PROPERTY (QString currencySymbol READ getCurrencySymbol NOTIFY currencySymbolChangedEvent)
    Q_PROPERTY (QString paymentAccount READ getPaymentAccount WRITE setPaymentAccount NOTIFY paymentAccountChangedEvent)
    Q_PROPERTY (QString payment2 READ getPayment2 WRITE setPayment2 NOTIFY payment2ChangedEvent)

public:
    QString getName() const;
    QString getVatNumber() const;
    QString getAddress() const;
    QString getPhoneNumber() const;
    QString getEmail() const;
    QString getPostalCode() const;
    QString getPostalArea() const;
    QString getCountry() const;
    QString getLogoPath() const;
    QString getLogoBase64() const;
    int getCurrencyID() const;
    QString getCurrencyCode() const;
    QString getCurrencySymbol() const;
    QString getPaymentAccount() const;
    QString getPayment2() const;

    void setName(QString arg);
    void setVatNumber(QString arg);
    void setAddress(QString arg);
    void setPhoneNumber(QString arg);
    void setEmail(QString arg);
    void setPostalCode(QString arg);
    void setPostalArea(QString arg);
    void setCountry(QString arg);
    void setLogoPath(QString arg);
    void setLogoBase64(QString arg);
    void setCurrencyID(int arg);
    void setPaymentAccount(QString arg);
    void setPayment2(QString arg);

    Q_INVOKABLE QString toCurrencyString(double value);

    Q_INVOKABLE void loadInfo();
    Q_INVOKABLE void saveInfo();

public slots:
    void onLogoImageSelected(QString filePath);

signals:
    void nameChangedEvent(QString arg);
    void vatNumberChangedEvent(QString arg);
    void addressChangedEvent(QString arg);
    void phoneNumberChangedEvent(QString arg);
    void emailChangedEvent(QString arg);
    void postalCodeChangedEvent(QString arg);
    void postalAreaChangedEvent(QString arg);
    void countryChangedEvent(QString arg);
    void logoPathChangedEvent(QString arg);
    void logoBase64ChangedEvent(QString arg);
    void currencyIDChangedEvent(int arg);
    void currencyCodeChangedEvent(QString arg);
    void currencySymbolChangedEvent(QString arg);
    void paymentAccountChangedEvent(QString arg);
    void payment2ChangedEvent(QString arg);
};

#endif // BusinessInfoModel_H
