import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Rectangle {
    id: root
    anchors.fill: parent

    signal completed();

    color: "#e5e4e2"

    property InvoiceModel invoiceModel

    property int selectedItemIndex

    CustomerSearchModel {
        id:customerSearchModel
    }

    // Hide customer suggestion list by default
    Component.onCompleted: customerSuggestionList.visible = false

    // Content: Customer form
    // Product list
    Rectangle {
        color: "#ffffff"
        height: childrenRect.height
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        radius: 10

        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.margins: 15

            RowLayout {
                anchors.right: parent.right
                anchors.left: parent.left
                // Customer name / customer search
                TextField {
                    id: customerText
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    placeholderText: qsTr("Customer") + " *"
                    text: invoiceModel.customerName
                    inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
                    onDisplayTextChanged: customerSearchModel.search(customerText.text)
                    onActiveFocusChanged: {
                        if(activeFocus) {
                            customerSuggestionList.visible = true
                            customerSearchModel.search(customerText.text)
                        }
                    }
                    onTextEdited: {
                        //invoiceModel.customerID = 0
                        customerSuggestionList.visible = true
                    }
                }
                // Add new customer button
                ToolButton {
                    Layout.alignment: Qt.AlignRight
                    icon.source: "../icons/plus_64x64.png"
                    icon.color: transparent
                    onClicked: {
                        var component = Qt.createComponent("CreateCustomerPage.qml");
                        pushPage(component)
                        getStackItem().onCompleted.connect(onCustomerCreated)
                    }
                }
            }

            // Customer search results
            ListView {
                id: customerSuggestionList
                anchors.right: parent.right
                anchors.left: parent.left
                height: childrenRect.height
                model: customerSearchModel
                delegate: ToolButton {
                    id: customerListButton
                    Layout.alignment: Qt.AlignLeft
                    text: id + ": " + name
                    contentItem: Text {
                        text: parent.text
                        font: parent.font
                        anchors.verticalCenter: parent.left
                    }
                    onClicked: {
                        invoiceModel.customerID = id
                        customerSuggestionList.visible = false
                        customerText.text = name;
                    }
                }
            }

            // Descritpion
            TextField {
                id: description
                width: parent.width
                Layout.preferredHeight: 40
                verticalAlignment: Text.AlignVCenter
                text: invoiceModel.description
                placeholderText: qsTr("Comment")
                Binding {
                    target: invoiceModel
                    property: "description"
                    value: description.text
                }
            }

            // Invoice dates
            RowLayout {
                width: parent.width
                Layout.preferredHeight: 40
                MouseArea {
                    width: childrenRect.width
                    height: childrenRect.height
                    Column {
                        Label {
                            Layout.fillWidth: true
                            text: invoiceModel.invoiceDate.toLocaleDateString(Locale.ShortFormat)
                        }
                        Label {
                            Layout.fillWidth: true
                            color: "#66CDAA"
                            text: qsTr("Invoice date")
                        }
                    }
                   onClicked: invoiceDatePicker.showDatePicker(invoiceModel.invoiceDate)
                }

                MouseArea {
                    width: childrenRect.width
                    height: childrenRect.height
                    Column {
                        Label {
                            Layout.fillWidth: true
                            text: invoiceModel.dueDate.toLocaleDateString(Locale.ShortFormat)
                        }
                        Label {
                            Layout.fillWidth: true
                            color: "#66CDAA"
                            text: qsTr("Due date")
                        }
                    }
                    onClicked: dueDatePicker.showDatePicker(invoiceModel.dueDate)
                }
            }

            Label {
                text: qsTr("Products")
                font.pixelSize: Qt.application.font.pixelSize * 1.2
                font.bold: true
            }

            // Product list
            ListView {
                id: itemList
                width: parent.width
                height: Math.min(childrenRect.height, root.height / 2)
                clip: true
                ScrollBar.vertical: ScrollBar {}

                onCountChanged: currentIndex = count - 1 // Scroll to end

                model: invoiceModel.products
                delegate: Item {
                    width: parent.width
                    height: childrenRect.height
                    RowLayout {
                        width: parent.width
                        // Icon
                        Image {
                            source: "../icons/trolley_64x64.png"
                            Layout.preferredHeight: 24
                            Layout.preferredWidth: 24
                            Layout.margins: 5
                            Layout.alignment: Qt.AlignLeft
                        }
                        // Product info
                        ColumnLayout {
                            width: parent.width
                            Label {
                                Layout.fillWidth: true
                                text: edit.name
                                font.pixelSize: Qt.application.font.pixelSize * 1.2
                                font.bold: true
                            }
                            RowLayout {
                                Layout.fillWidth: true
                                Label {
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignLeft
                                    text: edit.quantity + "x " + businessInfoModel.toCurrencyString(edit.totalPrice / edit.quantity)
                                    font.pixelSize: Qt.application.font.pixelSize * 1.2
                                    color: "grey"
                                }
                                Label {
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignRight
                                    horizontalAlignment: Text.AlignRight
                                    text: businessInfoModel.toCurrencyString(edit.totalPrice)
                                    font.pixelSize: Qt.application.font.pixelSize * 1.5
                                }
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            selectedItemIndex = index
                            var component = Qt.createComponent("InvoiceItemInfoPage.qml");
                            pushPage(component)
                            getStackItem().setItemData(edit.productID, edit.name, edit.unitPrice, edit.quantity, edit.discount, edit.tax)
                            getStackItem().onSaveItem.connect(updateSelectedInvoiceItem)
                            getStackItem().onDeleteItem.connect(removeSelectedInvoiceItem)
                        }
                    }
                }
            }

            ToolButton {
                id: addProduct
                icon.source: "../icons/plus_64x64.png"
                icon.color: transparent
                onClicked: {
                    var component = Qt.createComponent("InvoiceItemCreationPage.qml");
                    pushPage(component)
                    getStackItem().onCompleted.connect(addInvoiceItem)
                }
            }

            Rectangle {
                width: parent.width
                height: childrenRect.height * 1.2
                color: "lightGrey"
                RowLayout {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        text: qsTr("Total")
                    }
                    Label {
                        id: txtSumTotal
                        Layout.alignment: Qt.AlignRight
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        text: businessInfoModel.toCurrencyString(invoiceModel.sumTotal)
                    }
                }
            }
        }
    }

    function addInvoiceItem(id, name, unitPrice, quantity, discount, tax) {
        invoiceModel.products.addProduct(id, name, unitPrice, quantity, discount, tax)
        invoiceModel.onProductsChanged()
    }

    function updateSelectedInvoiceItem(id, name, unitPrice, quantity, discount, tax) {
        console.log(name)
        invoiceModel.products.updateProduct(selectedItemIndex, id, name, unitPrice, quantity, discount, tax)
        invoiceModel.onProductsChanged()
    }

    function removeSelectedInvoiceItem() {
        invoiceModel.products.removeProduct(selectedItemIndex)
        invoiceModel.onProductsChanged()
    }

    function onCustomerCreated(id, name)
    {
        invoiceModel.customerID = id
        customerText.text = name
        customerSuggestionList.visible = false
    }

    DatePickerPopup {
        id: invoiceDatePicker
        onCompleted: invoiceModel.invoiceDate = selectedDate
    }
    DatePickerPopup {
        id: dueDatePicker
        onCompleted: invoiceModel.dueDate = selectedDate
    }
}
