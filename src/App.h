#ifndef APP_H
#define APP_H

#include "UserDatabase.h"
#include "Models/BusinessInfoModel.h"
#include "Models/ApplicationModel.h"
#include <QApplication>
#include <QTranslator>
#include <QQmlApplicationEngine>
#include <QString>

class App
{
private:
    UserDatabase* mUserDatabase = nullptr;
    QApplication* mQtApp = nullptr;
    QQmlApplicationEngine* mAppEngine = nullptr;
    QTranslator* mTranslator = nullptr;
    QString mLangauge = "en-GB";

public:
    App();

    BusinessInfoModel* mBusinessModel = nullptr;
    ApplicationModel* mApplicationModel = nullptr;

    void initialiseApp(QApplication* app, QQmlApplicationEngine* engine);
    void setLanguage(QString langCode);
    void clearDatabase();

    UserDatabase* getUserDatabase() { return mUserDatabase; }
    QString getLanguage() { return mLangauge; }
};

extern App* GApp;

#endif // APP_H
