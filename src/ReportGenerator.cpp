#include "ReportGenerator.h"
#include "lrreportengine.h"
#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QLocale>

ReportGenerator::ReportGenerator()
{

}

ReportGenerator::~ReportGenerator()
{
    if(mReport != nullptr)
        delete mReport;
}

bool ReportGenerator::loadReport(QString reportPath)
{
    qDebug() << "Loading report: " << reportPath;

    mReport = new LimeReport::ReportEngine();
    mReport->dataManager()->setDefaultDatabasePath(QDir::currentPath());
    if(!mReport->loadFromFile(reportPath))
    {
        qDebug() << "Failed to load report: " << reportPath;
        return false;
    }
}

bool ReportGenerator::exportPDF(QString pdfPath)
{
    // Remove file if it already exists.
    if(QFile::exists(pdfPath))
        QFile::remove(pdfPath);

    if(!mReport->printToPDF(pdfPath))
    {
        qDebug() << "Failed to print to PDF: " << pdfPath;
        return false;
    }

    if(!QFile(pdfPath).exists())
    {
        qDebug() << "PDF could not be saved to: " << pdfPath;
        return false;
    }

    qDebug() << "Exported PDF to: " << pdfPath;
    return true;
}

void ReportGenerator::setReportLanguage(QLocale::Language lang)
{
    mReport->setReportLanguage(lang);
}

void ReportGenerator::setReportVariable(QString name, QVariant value)
{
    mReport->dataManager()->setReportVariable(name, value);
}
