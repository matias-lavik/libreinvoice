import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0
import QtQuick.Controls 1.4
import Qt.labs.platform 1.0
import QtQuick.Controls 2.15

Page {
    id: root
    anchors.fill: parent

    CurrenciesModel {
        id: currenciesModel
    }

    onVisibleChanged: {
        businessInfoModel.loadInfo();
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    property int selectedCurrencyId: businessInfoModel.currencyID

    header: ToolBar {
        id: toolBar
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            id: toolBarLayout
            Layout.alignment: Qt.AlignLeft
            width: parent.width

            ToolButton {
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    popPage()
                }
            }

            Label {
                text: qsTr("Settings")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }

            ToolButton {
                icon.source: "../icons/check_32x32.png"
                icon.color: transparent
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    saveAndClose()
                }
            }
        }
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent
        anchors.margins: 15

        Column {
            width: scrollView.width
            anchors.margins: 10
            spacing: 8

            Label {
                Layout.preferredWidth: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.8
                text: qsTr("Language")
            }

            LanguageSelectionBox {
                id: langSelect
                width: parent.width
            }

            Label {
                Layout.preferredWidth: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.8
                text: qsTr("Currency")
            }

            Button {
                id: currencyButton
                width: parent.width
                text: businessInfoModel.currencyCode + "(" + businessInfoModel.currencySymbol + ")"
                background: Rectangle {
                    color: "#ffffff"
                }
                onClicked: currencyPickerPopup.open()
            }
        }
    }

    CurrencyPickerPopup {
        id: currencyPickerPopup
        onCompleted: {
            selectedCurrencyId = currencyID
            currencyButton.text = currencyCode + "(" + currencySymbol + ")"
        }
    }

    function saveAndClose() {
        businessInfoModel.currencyID = selectedCurrencyId
        businessInfoModel.saveInfo()
        applicationModel.setLanguage(langSelect.selectedLanguage)
        popPage()
    }
}
