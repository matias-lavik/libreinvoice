#ifndef INVOICEMODEL_H
#define INVOICEMODEL_H

#include <QObject>
#include <QDate>
#include "InvoiceProductListModel.h"

class InvoiceModel : public QObject
{
    Q_OBJECT
public:
    explicit InvoiceModel(QObject *parent = nullptr);

private:
    int mInvoiceID = 0;
    int mCustomerID = 0;
    QDate mInvoicenDate;
    QDate mDueDate;
    QString mDescription;
    QString mCustomerName;
    double mSumTotal = 0.0;
    bool mIsPaid = 0.0;
    InvoiceProductListModel* mProductList = nullptr;

    bool mIsModified = false;

    Q_PROPERTY (int invoiceID READ getInvoiceID NOTIFY invoiceIDChangedEvent)
    Q_PROPERTY (int customerID READ getCustomerID WRITE setCustomerID NOTIFY customerIDChangedEvent)
    Q_PROPERTY (QDate invoiceDate READ getInvoiceDate WRITE setInvoiceDate NOTIFY invoiceDateChangedEvent)
    Q_PROPERTY (QDate dueDate READ getDueDate WRITE setDueDate NOTIFY dueDateChangedEvent)
    Q_PROPERTY (QString description READ getDescription WRITE setDescription NOTIFY descriptionChangedEvent)
    Q_PROPERTY (QString customerName READ getCustomerName WRITE setCustomerName NOTIFY customerNameChangedEvent)
    Q_PROPERTY (double sumTotal READ getSumTotal WRITE setSumTotal NOTIFY sumTotalChangedEvent)
    Q_PROPERTY (bool isPaid READ getIsPaid WRITE setIsPaid NOTIFY isPaidChangedEvent)
    Q_PROPERTY (InvoiceProductListModel* products READ getProducts)

    void saveProductList();

public:
    int getInvoiceID() const;
    int getCustomerID() const;
    QDate getInvoiceDate() const;
    QDate getDueDate() const;
    QString getDescription() const;
    QString getCustomerName() const;
    double getSumTotal() const;
    bool getIsPaid() const;
    InvoiceProductListModel* getProducts() const;

    Q_INVOKABLE bool isModified();

    void setInvoiceID(int arg);
    void setCustomerID(int arg);
    void setInvoiceDate(QDate arg);
    void setDueDate(QDate arg);
    void setDescription(QString arg);
    void setCustomerName(QString arg);
    void setSumTotal(double arg);
    void setIsPaid(bool arg);

    void setModified(bool modified);

    Q_INVOKABLE void loadInvoice(int id);
    Q_INVOKABLE void saveInvoice();
    Q_INVOKABLE void saveNewInvoice();
    Q_INVOKABLE void deleteInvoice();
    Q_INVOKABLE void exportReport(QString repLangCode);
    Q_INVOKABLE void onProductsChanged();

signals:
    void invoiceIDChangedEvent(int arg);
    void customerIDChangedEvent(int arg);
    void invoiceDateChangedEvent(QDate arg);
    void dueDateChangedEvent(QDate arg);
    void descriptionChangedEvent(QString arg);
    void customerNameChangedEvent(QString arg);
    void sumTotalChangedEvent(double arg);
    void isPaidChangedEvent(bool arg);
};

#endif // INVOICEMODEL_H
