import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick 2.15
import Qt.labs.calendar 1.0
import LibreInvoice 1.0

Popup {
    id: popup
    x: 0
    y: 0
    width: parent.width
    height: width
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    signal completed(int currencyID, string currencyCode, string currencyName, string currencySymbol);

    CurrenciesModel {
        id: currencyModel
    }

    TextField {
        id: searchText
        placeholderText: "søk"
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
        onDisplayTextChanged: currencyModel.filterBySearchString(searchText.text)
    }

    ScrollView {
        anchors.top: searchText.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: childrenRect.height

        ListView {
            anchors.fill: parent
            model: currencyModel
            delegate: Item {
                width: parent.width
                height: childrenRect.height
                Column {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    Label {
                        text: code + (symbol == "" ? "" : "(" + symbol + ")")
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        font.bold: true
                    }
                    Label {
                        text: name
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        color: "grey"
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        popup.completed(id, code, name, symbol)
                        popup.close()
                    }
                }
            }
        }
    }
}
