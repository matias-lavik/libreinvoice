#ifndef INVOICEPRODUCTLISTMODEL_H
#define INVOICEPRODUCTLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "InvoiceProductListItemModel.h"

class InvoiceProductListModel : public QAbstractListModel
{
    Q_OBJECT

private:
    QList<InvoiceProductListItemModel*> mProductEntries;
    bool mIsModified = false;

public:
    explicit InvoiceProductListModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE InvoiceProductListItemModel* addProduct(int id, QString name, double unitPrice, int quantity, double discount, double tax);
    Q_INVOKABLE void updateProduct(int index, int id, QString name, double unitPrice, int quantity, double discount, double tax);
    Q_INVOKABLE void removeProduct(int index);
    void setModified(bool modified);
    int size() const { return mProductEntries.size(); }

    QList<InvoiceProductListItemModel*> getProducts();
    bool isModified() { return mIsModified; }

signals:
    void sizeChanged(int arg);

signals:

};

#endif // INVOICEPRODUCTLISTMODEL_H
