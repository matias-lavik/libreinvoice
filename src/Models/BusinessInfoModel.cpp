#include "BusinessInfoModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QFile>
#include <QBuffer>
#include <QDir>
#include <QQmlFile>
#include <QFileDialog>
#include <QLocale>

BusinessInfoModel::BusinessInfoModel(QObject *parent) : QObject(parent)
{
    loadInfo();
}

QString BusinessInfoModel::getName() const
{
    return mName;
}

QString BusinessInfoModel::getVatNumber() const
{
    return mVatNumber;
}

QString BusinessInfoModel::getAddress() const
{
    return mAddress;
}

QString BusinessInfoModel::getPhoneNumber() const
{
    return mPhoneNumber;
}

QString BusinessInfoModel::getEmail() const
{
    return mEmail;
}

QString BusinessInfoModel::getPostalCode() const
{
    return mPostalCode;
}

QString BusinessInfoModel::getPostalArea() const
{
    return mPostalArea;
}

QString BusinessInfoModel::getCountry() const
{
    return mCountry;
}

QString BusinessInfoModel::getLogoPath() const
{
    return mLogoPath;
}

QString BusinessInfoModel::getLogoBase64() const
{
    return mLogoBase64;
}

int BusinessInfoModel::getCurrencyID() const
{
    return mCurrencyID;
}

QString BusinessInfoModel::getCurrencyCode() const
{
    return mCurrencyCode;
}

QString BusinessInfoModel::getCurrencySymbol() const
{
    return mCurrencySymbol;
}

QString BusinessInfoModel::getPaymentAccount() const
{
    return mPaymentAccount;
}


QString BusinessInfoModel::getPayment2() const
{
    return mPayment2;
}



void BusinessInfoModel::setName(QString arg)
{
    mName = arg;
    nameChangedEvent(arg);
}

void BusinessInfoModel::setVatNumber(QString arg)
{
    mVatNumber = arg;
    vatNumberChangedEvent(arg);
}

void BusinessInfoModel::setAddress(QString arg)
{
    mAddress = arg;
    addressChangedEvent(arg);
}

void BusinessInfoModel::setPhoneNumber(QString arg)
{
    mPhoneNumber = arg;
    phoneNumberChangedEvent(arg);
}

void BusinessInfoModel::setEmail(QString arg)
{
    mEmail = arg;
    emailChangedEvent(arg);
}

void BusinessInfoModel::setPostalCode(QString arg)
{
    mPostalCode = arg;
    postalCodeChangedEvent(arg);
}

void BusinessInfoModel::setPostalArea(QString arg)
{
    mPostalArea = arg;
    postalAreaChangedEvent(arg);
}

void BusinessInfoModel::setCountry(QString arg)
{
    mCountry = arg;
    countryChangedEvent(arg);
}

void BusinessInfoModel::setLogoPath(QString arg)
{
    qDebug() << "Setting logo path: " << arg;

    mLogoPath = QQmlFile::urlToLocalFileOrQrc(arg);
    logoPathChangedEvent(mLogoPath);

    // Read logo image, and convert to base64
    QByteArray logoArray = QByteArray();
    if(mLogoPath != "")
    {
        QFile logoFile(mLogoPath);
        if(logoFile.open(QIODevice::ReadOnly))
        {
            logoArray = logoFile.readAll();
            setLogoBase64(logoArray.toBase64());
        }
        else
            qDebug() << "Invalid logo: " << mLogoPath;
    }
}

void BusinessInfoModel::setLogoBase64(QString arg)
{
    mLogoBase64 = arg;
    logoBase64ChangedEvent(arg);
}

void BusinessInfoModel::setCurrencyID(int arg)
{
    mCurrencyID = arg;
    currencyIDChangedEvent(arg);

    // Get currency information
    QSqlQuery query(GApp->getUserDatabase()->getSqlDatabase());
    query.prepare("select code, symbol from currencies where id = ?");
    query.addBindValue(arg);
    query.exec();
    query.first();
    mCurrencyCode = query.value(0).toString();
    mCurrencySymbol = query.value(1).toString();
    currencyCodeChangedEvent(mCurrencyCode);
    currencySymbolChangedEvent(mCurrencySymbol);
}

void BusinessInfoModel::setPaymentAccount(QString arg)
{
    mPaymentAccount = arg;
    paymentAccountChangedEvent(arg);
}

void BusinessInfoModel::setPayment2(QString arg)
{
    mPayment2 = arg;
    payment2ChangedEvent(arg);
}

void BusinessInfoModel::onLogoImageSelected(QString filePath)
{
    QFile file(filePath);
    if (!file.open(QFile::ReadOnly))
        qDebug() << "FAILED: " << file.errorString();
    else
        qDebug() << "OK!!!";

    qDebug() << QQmlFile::urlToLocalFileOrQrc(filePath);

    setLogoPath(filePath);

    if (!filePath.isNull())
    {
        qDebug() << "FileName: " << filePath;
    }
    else
        qDebug() << "No logo file selected";
}

QString BusinessInfoModel::toCurrencyString(double value)
{
    // QTBUG-89383: Qt seems to use the wrong format for negative numbers.
    //  So some localse end up with negative numbers inside a parenthesis.
    // TODO: Wait for fix to QTBUG-89383 or find workaround.
    //return QLocale::system().toCurrencyString(value, mCurrencySymbol);
    QString curStr =  QLocale(QLocale::English, QLocale::Country::UnitedKingdom).toCurrencyString(value, mCurrencySymbol);
    if(value < 0.0 && curStr[0] == '(' && curStr[curStr.size() - 1] == ')')
        curStr = QString("-") + curStr.mid(1, curStr.size() - 2);
    return curStr;
}

void BusinessInfoModel::loadInfo()
{
    qDebug() << "Loading business info";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery query(db);
    query.prepare("select name, vatNumber, address, phoneNumber, email, postalCode, postalArea, country, logo, currencyID, paymentAccount, payment2 from businessInfo");
    query.exec();

    query.next();

    setName(query.value(0).toString());
    setVatNumber(query.value(1).toString());
    setAddress(query.value(2).toString());
    setPhoneNumber(query.value(3).toString());
    setEmail(query.value(4).toString());
    setPostalCode(query.value(5).toString());
    setPostalArea(query.value(6).toString());
    setCountry(query.value(7).toString());
    QByteArray logoBytes = query.value(8).toByteArray();
    setLogoBase64(logoBytes.toBase64());
    setCurrencyID(query.value(9).toInt());
    setPaymentAccount(query.value(10).toString());
    setPayment2(query.value(11).toString());
}

void BusinessInfoModel::saveInfo()
{
    qDebug() << "Saving business info";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Delete existing row
    QSqlQuery deleteQuery(db);
    deleteQuery.prepare("delete from businessInfo");
    deleteQuery.exec();

    QByteArray logoArray = QByteArray();
    if(mLogoPath != "")
    {
        QFile logoFile(mLogoPath);
        if(logoFile.open(QIODevice::ReadOnly))
            logoArray = logoFile.readAll();
        else
            qDebug() << "Invalid logo!";
    }
    else if(mLogoBase64 != "")
    {
        logoArray = QByteArray::fromBase64(mLogoBase64.toUtf8());
    }

    // Insert new info
    QSqlQuery insertQuery(db);
    insertQuery.prepare("insert into businessInfo(name, vatNumber, address, phoneNumber, email, postalCode, postalArea, country, logo, currencyID, paymentAccount, payment2) values(?,?,?,?,?,?,?,?,?,?,?,?)");
    insertQuery.addBindValue(mName);
    insertQuery.addBindValue(mVatNumber);
    insertQuery.addBindValue(mAddress);
    insertQuery.addBindValue(mPhoneNumber);
    insertQuery.addBindValue(mEmail);
    insertQuery.addBindValue(mPostalCode);
    insertQuery.addBindValue(mPostalArea);
    insertQuery.addBindValue(mCountry);
    insertQuery.addBindValue(logoArray);
    insertQuery.addBindValue(mCurrencyID);
    insertQuery.addBindValue(mPaymentAccount);
    insertQuery.addBindValue(mPayment2);
    insertQuery.exec();
}
