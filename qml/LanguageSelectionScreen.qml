import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

GridLayout {
    columns: 1
    rows: 3
    rowSpacing: 15
    Rectangle {
        id: langENGB
        width: childrenRect.width
        height: childrenRect.height
        Layout.alignment: Qt.AlignHCenter
        color: "transparent"
        Column {
            Image {
                source: "../icons/uk_64x64.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "English"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
            }
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                applicationModel.setLanguage("en-GB")
                onLanguageChanged()
            }
        }
    }

    Rectangle {
        id: langNBNO
        width: childrenRect.width
        height: childrenRect.height
        Layout.alignment: Qt.AlignHCenter
        color: "transparent"
        Column {
            Image {
                source: "../icons/norway_64x64.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Norsk Bokmål"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
            }
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                applicationModel.setLanguage("nb-NO")
                onLanguageChanged()
            }
        }
    }

    Rectangle {
        id: langNNNO
        width: childrenRect.width
        height: childrenRect.height
        Layout.alignment: Qt.AlignHCenter
        color: "transparent"
        Column {
            Image {
                source: "../icons/norway_64x64.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Norsk Nynorsk"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
            }
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                applicationModel.setLanguage("nn-NO")
                onLanguageChanged()
            }
        }
    }

    function onLanguageChanged() {
        langENGB.color = "#00000000"
        langNBNO.color = "#00000000"
        langNNNO.color = "#00000000"

        var lang = applicationModel.getLanguage()
        if(lang === "en-GB")
            langENGB.color = "#add8e6"
        else if(lang === "nb-NO")
            langNBNO.color = "#add8e6"
        else if(lang === "nn-NO")
            langNNNO.color = "#add8e6"
    }
}
