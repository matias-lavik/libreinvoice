#ifndef CURRENCIESMODEL_H
#define CURRENCIESMODEL_H

#include <QSqlTableModel>

class CurrenciesModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    explicit CurrenciesModel(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void filterByID(int currencyID);
    Q_INVOKABLE void filterBySearchString(QString searchString);
};

#endif // CURRENCIESMODEL_H
