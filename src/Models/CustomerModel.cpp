#include "CustomerModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QSqlError>

CustomerModel::CustomerModel(QObject *parent) : QObject(parent)
{

}

int CustomerModel::getCustomerID() const
{
    return mCustomerID;
}

QString CustomerModel::getName() const
{
    return mName;
}

QString CustomerModel::getVatNumber() const
{
    return mVatNumber;
}

QString CustomerModel::getAddress() const
{
    return mAddress;
}

QString CustomerModel::getPhoneNumber() const
{
    return mPhoneNumber;
}

QString CustomerModel::getEmail() const
{
    return mEmail;
}

QString CustomerModel::getPostalCode() const
{
    return mPostalCode;
}

QString CustomerModel::getPostalArea() const
{
    return mPostalArea;
}

QString CustomerModel::getCountry() const
{
    return mCountry;
}

void CustomerModel::setCustomerID(int arg)
{
    mIsModified |= mCustomerID != arg;
    mCustomerID = arg;
    customerIDChangedEvent(arg);
}

void CustomerModel::setName(QString arg)
{
    mIsModified |= mName != arg;
    mName = arg;
    nameChangedEvent(arg);
}

void CustomerModel::setVatNumber(QString arg)
{
    mIsModified |= mVatNumber != arg;
    mVatNumber = arg;
    vatNumberChangedEvent(arg);
}

void CustomerModel::setAddress(QString arg)
{
    mIsModified |= mAddress != arg;
    mAddress = arg;
    addressChangedEvent(arg);
}

void CustomerModel::setPhoneNumber(QString arg)
{
    mIsModified |= mPhoneNumber != arg;
    mPhoneNumber = arg;
    phoneNumberChangedEvent(arg);
}

void CustomerModel::setEmail(QString arg)
{
    mIsModified |= mEmail != arg;
    mEmail = arg;
    emailChangedEvent(arg);
}

void CustomerModel::setPostalCode(QString arg)
{
    mIsModified |= mPostalCode != arg;
    mPostalCode = arg;
    postalCodeChangedEvent(arg);
}

void CustomerModel::setPostalArea(QString arg)
{
    mIsModified |= mPostalArea != arg;
    mPostalArea = arg;
    postalAreaChangedEvent(arg);
}

void CustomerModel::setCountry(QString arg)
{
    mIsModified |= mCountry != arg;
    mCountry = arg;
    countryChangedEvent(arg);
}

void CustomerModel::loadCustomer(int customerID)
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery query(db);
    query.prepare("select id, name, vatNumber, address, phoneNumber, email, postalCode, postalArea, country from customers where id = ?");
    query.addBindValue(customerID);
    bool succeeded = query.exec();
    if(succeeded && query.first())
    {
        int id = query.value(0).toInt();
        QString name = query.value(1).toString();
        QString vatNumber = query.value(2).toString();
        QString address = query.value(3).toString();
        QString phoneNumber = query.value(4).toString();
        QString email = query.value(5).toString();
        QString postalCode = query.value(6).toString();
        QString postalArea = query.value(7).toString();
        QString country = query.value(8).toString();

        setCustomerID(id);
        setName(name);
        setVatNumber(vatNumber);
        setAddress(address);
        setPhoneNumber(phoneNumber);
        setEmail(email);
        setPostalCode(postalCode);
        setPostalArea(postalArea);
        setCountry(country);

        setModified(false);
    }
    else
        qDebug() << "Failed to query customer: " << customerID;

}

void CustomerModel::saveCustomer()
{
    qDebug() << "Saving customer";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery existsQuery(db);
    existsQuery.prepare("select id from customers where id = ?");
    existsQuery.addBindValue(mCustomerID);
    existsQuery.exec();
    if(existsQuery.first())
    {
        // Update existing customer
        QSqlQuery updateQuery(db);
        updateQuery.prepare("update customers set name = ?, vatNumber = ?, address = ?, phoneNumber = ?, email = ?, postalCode = ?, postalArea = ?, country = ? where id = ?");
        updateQuery.addBindValue(mName);
        updateQuery.addBindValue(mVatNumber);
        updateQuery.addBindValue(mAddress);
        updateQuery.addBindValue(mPhoneNumber);
        updateQuery.addBindValue(mEmail);
        updateQuery.addBindValue(mPostalCode);
        updateQuery.addBindValue(mPostalArea);
        updateQuery.addBindValue(mCountry);
        updateQuery.addBindValue(mCustomerID);
        updateQuery.exec();
    }
    else
    {
        // Save new customer
        saveNewCustomer();
    }

    setModified(false);
}

void CustomerModel::saveNewCustomer()
{
    qDebug() << "Saving customer";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery insertQuery(db);
    insertQuery.prepare("insert into customers(name, vatNumber, address, phoneNumber, email, postalCode, postalArea, country) values(?,?,?,?,?,?,?,?)");
    insertQuery.addBindValue(mName);
    insertQuery.addBindValue(mVatNumber);
    insertQuery.addBindValue(mAddress);
    insertQuery.addBindValue(mPhoneNumber);
    insertQuery.addBindValue(mEmail);
    insertQuery.addBindValue(mPostalCode);
    insertQuery.addBindValue(mPostalArea);
    insertQuery.addBindValue(mCountry);
    insertQuery.exec();

    QSqlQuery customerIDQuery(db);
    customerIDQuery.exec("select max(id) from customers");
    customerIDQuery.next();
    int customerID = customerIDQuery.value(0).toInt();
    setCustomerID(customerID);

    setModified(false);
}

void CustomerModel::deleteCustomer()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery deleteQuery(db);
    deleteQuery.prepare("delete from customers where id = ?");
    deleteQuery.addBindValue(mCustomerID);
    if(!deleteQuery.exec())
        qDebug() << "Failed to delete customer: " << deleteQuery.lastError();
}

void CustomerModel::setModified(bool modified)
{
    mIsModified = modified;
}

bool CustomerModel::isModified()
{
    return mIsModified;
}
