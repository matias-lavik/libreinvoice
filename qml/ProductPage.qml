import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0

Page {
    id: productPage
    anchors.fill: parent

    property bool isSearching: false

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight * 2 // TODO
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            id: toolBarLayout
            spacing: 0
            Layout.alignment: Qt.AlignLeft
            width: parent.width

            ToolButton {
                id: toolButton
                icon.source: "../icons/menu_64x64.png"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    drawer.open()
                }
            }

            Label {
                text: qsTr("Products")
                visible: !isSearching
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            TextField {
                id: searchText
                visible: isSearching
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
                placeholderText: qsTr("Search")
                font.pixelSize: Qt.application.font.pixelSize * 1.3
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                color: "black"
                onTextChanged:productList.setSearchString(searchText.text)
                background: Rectangle {
                    color: "#FFFFFF"
                }
            }

            ToolButton {
                icon.source: isSearching ? "../icons/remove_64x64.png" : "/icons/search_64x64.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    isSearching = !isSearching
                    searchText.clear()
                    if(isSearching)
                        searchText.forceActiveFocus()
                }
            }
        }

        TabBar {
            anchors.top: toolBarLayout.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            background: Rectangle {
                color: "#66CDAA"
            }
            TabButton {
                text: qsTr("All")
            }
        }
    }

    Rectangle {
        id: listBackground
        color: "#ffffff"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        ProductList {
            id: productList
            onSelectProduct: showProduct(id)
        }
    }

    ToolButton {
        id: btnNewProduct
        icon.source: "../icons/plus_64x64.png"
        icon.color: transparent
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        icon.width: 64
        icon.height: 64
        onClicked: {
            var component = Qt.createComponent("CreateProductPage.qml");
            pushPage(component)
        }
    }

    function onCreateCustomer()
    {
        stackView.pop()
    }

    function showProduct(productID) {
        var component = Qt.createComponent("ProductInfoPage.qml");
        pushPage(component)
        getStackItem().loadProduct(productID)
    }
}
