#include "StandardPaths.h"
#include <QDir>
#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

StandardPaths::StandardPaths()
{
}

QString StandardPaths::getAssetsPath()
{
#ifdef Q_OS_ANDROID
    return "assets:";
#else
    return "assets";
#endif
}

QString StandardPaths::getReportsPath()
{
    return QDir::cleanPath(getAssetsPath() + "/reports");
}
