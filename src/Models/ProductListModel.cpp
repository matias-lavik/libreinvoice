#include "ProductListModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlRecord>

const QString SQL_STRING_BASE = "select id, name, price, tax from products";

ProductListModel::ProductListModel(QObject *parent) : QSqlQueryModel(parent)
{
    refresh();
}

QVariant ProductListModel::data(const QModelIndex &index, int role) const
{
    if(role < Qt::UserRole)
        return QSqlQueryModel::data(index, role);
    QSqlRecord r = record(index.row());
    QVariant var = r.value(QString(roleNames().value(role))).toString();
    return var;
}

QHash<int, QByteArray> ProductListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Qt::UserRole] = "id";
    names[Qt::UserRole + 1] = "name";
    names[Qt::UserRole + 2] = "price";
    names[Qt::UserRole + 3] = "tax";
    return names;
}

void ProductListModel::setSearchString(QString searchString)
{
    mSearchString = QString("name like \"\%%1\%\"").arg(searchString);
    refresh();
}

void ProductListModel::refresh()
{
    QString sqlString = SQL_STRING_BASE;
    if(mSearchString != "")
        sqlString += " where " + mSearchString;
    setQuery(sqlString, GApp->getUserDatabase()->getSqlDatabase());
}
