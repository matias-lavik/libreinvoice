import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Page {
    id: invoicePage
    anchors.fill: parent

    signal onShowDrawer();

    property bool isSearching: false

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight * 2 // TODO
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            id: toolBarLayout
            spacing: 0
            Layout.alignment: Qt.AlignLeft
            width: parent.width

            ToolButton {
                id: toolButton
                icon.source: "../icons/menu_64x64.png"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    drawer.open()
                }
            }

            Label {
                text: qsTr("Invoice")
                visible: !isSearching
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            TextField {
                id: searchText
                visible: isSearching
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
                placeholderText: qsTr("Search")
                font.pixelSize: Qt.application.font.pixelSize * 1.3
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                color: "black"
                onTextChanged:invoiceList.setSearchString(searchText.text)
                background: Rectangle {
                    color: "#FFFFFF"
                }
            }

            ToolButton {
                icon.source: isSearching ? "../icons/remove_64x64.png" : "/icons/search_64x64.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    isSearching = !isSearching
                    searchText.clear()
                    if(isSearching)
                        searchText.forceActiveFocus()
                }
            }
        }
        TabBar {
            anchors.top: toolBarLayout.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            background: Rectangle {
                color: "#66CDAA"
            }
            TabButton {
                text: qsTr("All")
                onClicked: invoiceList.showAll()
            }
            TabButton {
                text: qsTr("Paid")
                onClicked: invoiceList.showPaid()()
            }
            TabButton {
                text: qsTr("Unpaid")
                onClicked: invoiceList.showUnpaid()
            }
        }
    }

    Rectangle {
        id: listBackground
        color: "#ffffff"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        InvoiceList {
            id: invoiceList
        }
    }

    ToolButton {
        id: btnNewCustomer
        icon.source: "../icons/plus_64x64.png"
        icon.color: transparent
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        icon.width: 64
        icon.height: 64
        onClicked: {
            var component = Qt.createComponent("CreateInvoicePage.qml");
            pushPage(component)
        }
    }

    function showInvoice(invoiceID) {
        var component = Qt.createComponent("InvoiceInfoPage.qml");
        pushPage(component)
        getStackItem().loadInvoice(invoiceID)
    }
}
