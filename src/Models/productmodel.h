#ifndef PRODUCTMODEL_H
#define PRODUCTMODEL_H

#include <QObject>

class ProductModel : public QObject
{
    Q_OBJECT
public:
    explicit ProductModel(QObject *parent = nullptr);

private:
    int mProductID = 0;
    QString mProductName;
    double mPrice = 0.0;
    double mTax = 0.0f;
    bool mIsModified = false;

    Q_PROPERTY (int productID READ getProductID NOTIFY productIDChangedEvent)
    Q_PROPERTY (QString productName READ getProductName WRITE setProductName NOTIFY productNameChangedEvent)
    Q_PROPERTY (double price READ getPrice WRITE setPrice NOTIFY priceChangedEvent)
    Q_PROPERTY (double tax READ getTax WRITE setTax NOTIFY taxChangedEvent)

public:
    int getProductID() const;
    QString getProductName() const;
    double getPrice() const;
    double getTax() const;

    void setProductID(int arg);
    void setProductName(QString arg);
    void setPrice(double arg);
    void setTax(double arg);

    Q_INVOKABLE void loadProduct(int id);
    Q_INVOKABLE void saveProduct();
    Q_INVOKABLE void saveNewProduct();
    Q_INVOKABLE void deleteProduct();

    Q_INVOKABLE void setModified(bool modified);
    Q_INVOKABLE bool isModified();

signals:
    void productIDChangedEvent(int arg);
    void productNameChangedEvent(QString arg);
    void priceChangedEvent(double arg);
    void taxChangedEvent(double arg);
};

#endif // PRODUCTMODEL_H
