import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Rectangle {
    id: root
    anchors.fill: parent

    signal completed();

    color: "#e5e4e2"

    property CustomerModel customerModel

    ScrollView {
        id: scrollView
        anchors.fill: parent
        anchors.margins: 15

        Column {
            id: columnLayout
            width: parent.width
            spacing: 8

            // Customer info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5
                    TextField {
                        id: customerName
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Name") + " *"
                        text: customerModel.name
                        Binding {
                            target: customerModel
                            property: "name"
                            value: customerName.text
                        }
                    }

                    TextField {
                        id: customerVAT
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("VAT number")
                        text: customerModel.vatNumber
                        Binding {
                            target: customerModel
                            property: "vatNumber"
                            value: customerVAT.text
                        }
                    }
                }
            }

            // Contact info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5

                    TextField {
                        id: customerPhone
                        anchors.right: parent.right
                        anchors.left: parent.left
                        inputMethodHints: Qt.ImhPreferNumbers
                        placeholderText: qsTr("Phone number")
                        text: customerModel.phoneNumber
                        Binding {
                            target: customerModel
                            property: "phoneNumber"
                            value: customerPhone.text
                        }
                    }

                    TextField {
                        id: customerEmail
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Email")
                        text: customerModel.email
                        Binding {
                            target: customerModel
                            property: "email"
                            value: customerEmail.text
                        }
                    }
                }
            }

            // Address
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5

                    TextField {
                        id: customerAddress
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Address")
                        text: customerModel.address
                        Binding {
                            target: customerModel
                            property: "address"
                            value: customerAddress.text
                        }
                    }

                    TextField {
                        id: customerPostalCode
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Postal code")
                        text: customerModel.postalCode
                        Binding {
                            target: customerModel
                            property: "postalCode"
                            value: customerPostalCode.text
                        }
                    }

                    TextField {
                        id: customerPostalArea
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Postal area")
                        text: customerModel.postalArea
                        Binding {
                            target: customerModel
                            property: "postalArea"
                            value: customerPostalArea.text
                        }
                    }

                    TextField {
                        id: customerCountry
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Country")
                        text: customerModel.country
                        Binding {
                            target: customerModel
                            property: "country"
                            value: customerCountry.text
                        }
                    }
                }
            }
        }
    }
}
