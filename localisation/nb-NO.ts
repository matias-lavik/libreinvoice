<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb-NO">
<context>
    <name>AndroidShareUtils</name>
    <message>
        <location filename="../src/shareutils/androidshareutils.cpp" line="184"/>
        <source>Share: an Error occured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shareutils/androidshareutils.cpp" line="195"/>
        <source>Share: an Error occured
WorkingDir not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shareutils/androidshareutils.cpp" line="219"/>
        <location filename="../src/shareutils/androidshareutils.cpp" line="244"/>
        <source>Empty URL received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shareutils/androidshareutils.cpp" line="236"/>
        <source>File does not exist: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BusinessForm</name>
    <message>
        <location filename="../qml/BusinessForm.qml" line="39"/>
        <source>Business information</source>
        <translation type="unfinished">Selskapsinformasjon</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="57"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="70"/>
        <source>VAT number</source>
        <translation>Organisasjonsnummer</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="99"/>
        <source>Phone number</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="112"/>
        <source>Email</source>
        <translation>Epost</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="140"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="153"/>
        <source>Postal code</source>
        <translation>Postnr</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="166"/>
        <source>Postal area</source>
        <translation>Poststed</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="179"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="193"/>
        <source>Currency</source>
        <translation type="unfinished">Valuta</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="214"/>
        <source>Payment information</source>
        <translation type="unfinished">Betalingsinformasjon</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="234"/>
        <source>Account number</source>
        <translation>Kontonr</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="247"/>
        <source>Alternative payment details</source>
        <translation>Betalingsalternativ 2</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="261"/>
        <source>Logo</source>
        <translation type="unfinished">Logo</translation>
    </message>
    <message>
        <location filename="../qml/BusinessForm.qml" line="267"/>
        <source>Upload logo</source>
        <translation type="unfinished">Last opp logo</translation>
    </message>
</context>
<context>
    <name>BusinessPage</name>
    <message>
        <location filename="../qml/BusinessPage.qml" line="49"/>
        <source>Business</source>
        <translation type="unfinished">Selskap</translation>
    </message>
</context>
<context>
    <name>CreateCustomerPage</name>
    <message>
        <location filename="../qml/CreateCustomerPage.qml" line="41"/>
        <source>New customer</source>
        <translation>Ny kunde</translation>
    </message>
</context>
<context>
    <name>CreateInvoicePage</name>
    <message>
        <location filename="../qml/CreateInvoicePage.qml" line="38"/>
        <source>New invoice</source>
        <translation>Ny faktura</translation>
    </message>
    <message>
        <location filename="../qml/CreateInvoicePage.qml" line="50"/>
        <source>A customer must be selected.&lt;br/&gt;Click the + button to create a new customer.</source>
        <translation type="unfinished">Du må velge en kunde først.&lt;br/&gt;Trykk på + knappen for å opprette ny kunde.</translation>
    </message>
</context>
<context>
    <name>CreateProductPage</name>
    <message>
        <location filename="../qml/CreateProductPage.qml" line="41"/>
        <source>New product</source>
        <translation type="unfinished">Ny vare</translation>
    </message>
    <message>
        <location filename="../qml/CreateProductPage.qml" line="54"/>
        <source>Product name is required.</source>
        <translation type="unfinished">Produktnavn mangler.</translation>
    </message>
</context>
<context>
    <name>CustomerForm</name>
    <message>
        <location filename="../qml/CustomerForm.qml" line="43"/>
        <source>Name</source>
        <translation type="unfinished">Navn</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="56"/>
        <source>VAT number</source>
        <translation type="unfinished">Organisasjonsnummer</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="85"/>
        <source>Phone number</source>
        <translation type="unfinished">Telefonr</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="98"/>
        <source>Email</source>
        <translation type="unfinished">Epost</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="126"/>
        <source>Address</source>
        <translation type="unfinished">Adresse</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="139"/>
        <source>Postal code</source>
        <translation type="unfinished">Postnr</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="152"/>
        <source>Postal area</source>
        <translation type="unfinished">Poststed</translation>
    </message>
    <message>
        <location filename="../qml/CustomerForm.qml" line="165"/>
        <source>Country</source>
        <translation type="unfinished">Land</translation>
    </message>
</context>
<context>
    <name>CustomerPage</name>
    <message>
        <location filename="../qml/CustomerPage.qml" line="39"/>
        <source>Customers</source>
        <translation type="unfinished">Kunder</translation>
    </message>
    <message>
        <location filename="../qml/CustomerPage.qml" line="49"/>
        <source>Search</source>
        <translation type="unfinished">Søk</translation>
    </message>
    <message>
        <location filename="../qml/CustomerPage.qml" line="82"/>
        <source>All</source>
        <translation type="unfinished">Alle</translation>
    </message>
</context>
<context>
    <name>EditCustomerPage</name>
    <message>
        <location filename="../qml/EditCustomerPage.qml" line="38"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">Lagre endringer?</translation>
    </message>
    <message>
        <location filename="../qml/EditCustomerPage.qml" line="78"/>
        <source>Save</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../qml/EditCustomerPage.qml" line="85"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>InvoiceExportDialog</name>
    <message>
        <location filename="../qml/InvoiceExportDialog.qml" line="27"/>
        <source>Select language for invoice</source>
        <translation type="unfinished">Velg språk for faktura</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceExportDialog.qml" line="40"/>
        <source>Send invoice</source>
        <translation type="unfinished">Send faktura</translation>
    </message>
</context>
<context>
    <name>InvoiceForm</name>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="49"/>
        <source>Customer</source>
        <translation type="unfinished">Kunde</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="108"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentar</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="131"/>
        <source>Invoice date</source>
        <translation type="unfinished">Fakturadato</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="148"/>
        <source>Due date</source>
        <translation type="unfinished">Forfallsdato</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="156"/>
        <source>Products</source>
        <translation type="unfinished">Varer</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceForm.qml" line="249"/>
        <source>Total</source>
        <translation type="unfinished">Sum</translation>
    </message>
</context>
<context>
    <name>InvoiceInfoPage</name>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="40"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">Lagre endringer?</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="47"/>
        <source>Invoice no </source>
        <translation type="unfinished">Fakturanr</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="91"/>
        <source>Save</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="100"/>
        <source>Send</source>
        <translation type="unfinished">Send</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="108"/>
        <source>Mark unpaid</source>
        <translation type="unfinished">Merk som ubetalt</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="108"/>
        <source>Mark paid</source>
        <translation type="unfinished">Merk som betalt</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceInfoPage.qml" line="117"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>InvoiceItemCreationPage</name>
    <message>
        <location filename="../qml/InvoiceItemCreationPage.qml" line="39"/>
        <source>Product</source>
        <translation type="unfinished">Vare</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemCreationPage.qml" line="76"/>
        <source>Add</source>
        <translation type="unfinished">Legg til</translation>
    </message>
</context>
<context>
    <name>InvoiceItemForm</name>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="27"/>
        <source>Product name</source>
        <translation type="unfinished">Varenavn</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="37"/>
        <source>Price</source>
        <translation type="unfinished">Pris</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="56"/>
        <source>Quantity</source>
        <translation type="unfinished">Antall</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="75"/>
        <source>Discount</source>
        <translation type="unfinished">Rabatt</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="94"/>
        <source>Tax</source>
        <translation type="unfinished">Mva</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemForm.qml" line="113"/>
        <source>Total (tax included)</source>
        <translation type="unfinished">Sum (inkl. mva)</translation>
    </message>
</context>
<context>
    <name>InvoiceItemInfoPage</name>
    <message>
        <location filename="../qml/InvoiceItemInfoPage.qml" line="36"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">Lagre endringer?</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemInfoPage.qml" line="43"/>
        <source>Product</source>
        <translation type="unfinished">Vare</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemInfoPage.qml" line="86"/>
        <source>Save</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../qml/InvoiceItemInfoPage.qml" line="94"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>InvoicePage</name>
    <message>
        <location filename="../qml/InvoicePage.qml" line="39"/>
        <source>Invoice</source>
        <translation type="unfinished">Faktura</translation>
    </message>
    <message>
        <location filename="../qml/InvoicePage.qml" line="50"/>
        <source>Search</source>
        <translation type="unfinished">Søk</translation>
    </message>
    <message>
        <location filename="../qml/InvoicePage.qml" line="82"/>
        <source>All</source>
        <translation type="unfinished">Alle</translation>
    </message>
    <message>
        <location filename="../qml/InvoicePage.qml" line="86"/>
        <source>Paid</source>
        <translation type="unfinished">Betalt</translation>
    </message>
    <message>
        <location filename="../qml/InvoicePage.qml" line="90"/>
        <source>Unpaid</source>
        <translation type="unfinished">Ubetalt</translation>
    </message>
</context>
<context>
    <name>ProductForm</name>
    <message>
        <location filename="../qml/ProductForm.qml" line="39"/>
        <source>Name</source>
        <translation type="unfinished">Navn</translation>
    </message>
    <message>
        <location filename="../qml/ProductForm.qml" line="48"/>
        <source>Price</source>
        <translation type="unfinished">Pris</translation>
    </message>
    <message>
        <location filename="../qml/ProductForm.qml" line="66"/>
        <source>Tax</source>
        <translation type="unfinished">Mva</translation>
    </message>
    <message>
        <location filename="../qml/ProductForm.qml" line="85"/>
        <source>Price (tax incl.)</source>
        <translation type="unfinished">Pris (inkl. mva)</translation>
    </message>
</context>
<context>
    <name>ProductInfoPage</name>
    <message>
        <location filename="../qml/ProductInfoPage.qml" line="38"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">Lagre endringer?</translation>
    </message>
    <message>
        <location filename="../qml/ProductInfoPage.qml" line="75"/>
        <source>Save</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../qml/ProductInfoPage.qml" line="82"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>ProductPage</name>
    <message>
        <location filename="../qml/ProductPage.qml" line="38"/>
        <source>Products</source>
        <translation type="unfinished">Varer</translation>
    </message>
    <message>
        <location filename="../qml/ProductPage.qml" line="49"/>
        <source>Search</source>
        <translation type="unfinished">Søk</translation>
    </message>
    <message>
        <location filename="../qml/ProductPage.qml" line="82"/>
        <source>All</source>
        <translation type="unfinished">Alle</translation>
    </message>
</context>
<context>
    <name>ProductSearchPage</name>
    <message>
        <location filename="../qml/ProductSearchPage.qml" line="36"/>
        <source>Search product</source>
        <translation type="unfinished">Finn vare</translation>
    </message>
    <message>
        <location filename="../qml/ProductSearchPage.qml" line="54"/>
        <source>Search</source>
        <translation type="unfinished">Søk</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/SettingsPage.qml" line="49"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="79"/>
        <source>Language</source>
        <translation type="unfinished">Språk</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="90"/>
        <source>Currency</source>
        <translation type="unfinished">Valuta</translation>
    </message>
</context>
<context>
    <name>SetupPage</name>
    <message>
        <location filename="../qml/SetupPage.qml" line="59"/>
        <source>Done</source>
        <translation type="unfinished">Ferdig</translation>
    </message>
    <message>
        <location filename="../qml/SetupPage.qml" line="59"/>
        <source>Next</source>
        <translation type="unfinished">Neste</translation>
    </message>
    <message>
        <location filename="../qml/SetupPage.qml" line="89"/>
        <source>Select language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/SetupPage.qml" line="106"/>
        <source>Fill in information about your business</source>
        <translation type="unfinished">Fyll ut informasjon om ditt selskap</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="12"/>
        <source>LibreInvoice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="74"/>
        <source>Business</source>
        <translation type="unfinished">Selskap</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="85"/>
        <source>Customers</source>
        <translation type="unfinished">Kunder</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="96"/>
        <source>Products</source>
        <translation type="unfinished">Varer</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>Invoices</source>
        <translation type="unfinished">Faktura</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="118"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="213"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="222"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="224"/>
        <source>No</source>
        <translation type="unfinished">Nei</translation>
    </message>
</context>
</TS>
