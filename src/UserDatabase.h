#ifndef USERDATABASE_H
#define USERDATABASE_H

#include <QSqlDatabase>
#include <QString>

class UserDatabase
{
private:
    QSqlDatabase mSqlDatabase;
    QString mDatabaseFilepath;

    void insertCurrencies();
    void insertBusiness();
    void insertSettings();

public:
    UserDatabase();

    bool createDatabase(QString filePath, QString dbname);
    bool loadDatabase(QString filePath, QString dbname);

    QSqlDatabase& getSqlDatabase() { return mSqlDatabase; }
};

#endif // USERDATABASE_H
