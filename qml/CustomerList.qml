import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0

Item {
    id: root
    anchors.fill: parent
    anchors.leftMargin: 10
    anchors.rightMargin: 10
    anchors.topMargin: 5
    anchors.bottomMargin: 5

    CustomerListViewModel {
        id: customerListViewModel
    }

    onVisibleChanged: {
        customerListViewModel.refreshList();
    }

    ListView {
        width: parent.width;
        height: parent.height

        model: customerListViewModel
        delegate: Rectangle {
            width: parent.width
            height: childrenRect.height
            RowLayout {
                width: parent.width
                layoutDirection: Qt.LeftToRight
                Image {
                    source: "../icons/user_32x32.png"
                    Layout.preferredHeight: 32
                    Layout.preferredWidth: 32
                    Layout.alignment: Qt.AlignLeft
                }
                ColumnLayout {
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 15
                    Layout.preferredWidth: parent.width * 0.6
                    Label {
                        text: edit.name
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        Layout.maximumWidth: parent.Layout.preferredWidth
                        clip: true
                        elide: Text.ElideRight
                        color: "black"
                    }
                    Label {
                        text: edit.phoneNumber
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        color: "darkgray"
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    showCustomer(edit.customerID)
                }
            }
        }
    }

    function setSearchString(searchString) {
        customerListViewModel.setSearchString(searchString)
    }
}
