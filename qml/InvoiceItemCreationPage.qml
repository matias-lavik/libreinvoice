import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal completed(int id, string name, double unitPrice, int quantity, double discount, double tax)

    background: Rectangle {
        color: "#e5e4e2"
    }

    // Toolbar
    header: ToolBar {
        contentHeight: btnBack.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            width: parent.width
            ToolButton {
                id: btnBack
                Layout.alignment: Qt.AlignLeft
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    popPage()
                }
            }

            Label {
                text: qsTr("Product")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            ToolButton {
                icon.source: "../icons/search_64x64.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    var component = Qt.createComponent("ProductSearchPage.qml");
                    pushPage(component)
                    getStackItem().onSelectProduct.connect(onProductSearchSelection)
                }
            }
        }
    }

    Rectangle {
        color: "#ffffff"
        anchors.right: parent.right
        anchors.left: parent.left
        height: childrenRect.height

        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            InvoiceItemForm {
                id: invoiceItemForm
                productModel: InvoiceProductListItemModel {
                    id: productModel
                }
            }

            Button {
                text: qsTr("Add")
                anchors.right: parent.right
                anchors.left: parent.left
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                background: Rectangle {
                    color: "#ffffff"
                    border.width: 1
                    border.color: "black"
                }
                onClicked: {
                    root.completed(invoiceItemForm.productModel.productID, invoiceItemForm.productModel.name, invoiceItemForm.productModel.unitPrice, invoiceItemForm.productModel.quantity, invoiceItemForm.productModel.discount, invoiceItemForm.productModel.tax)
                    popPage()
                }
            }
        }
    }

    function onProductSearchSelection(prodID) {
        invoiceItemForm.productModel.setProduct(prodID)
        invoiceItemForm.setFieldsFromModel()
    }
}
